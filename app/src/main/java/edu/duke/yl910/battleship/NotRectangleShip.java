package edu.duke.yl910.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class NotRectangleShip<T> extends BasicShip<T>{

    final String name;

    public NotRectangleShip(String name, Placement where,T data, T onHit){
        this(name,makeV2Coords(name,where),new SimpleShipDisplayInfo<>(data,onHit),
                new SimpleShipDisplayInfo<>(null,data));
    }

    public NotRectangleShip(String name, HashMap<Coordinate,Integer> record,
                            ShipDisplayInfo<T> myDisplayInfo,
                            ShipDisplayInfo<T> enemyDisplayInfo) {
        super(name,record, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    static HashMap<Coordinate, Integer> makeV2Coords(String name, Placement where){
        HashMap<Coordinate, Integer> map = new HashMap<>();
        int r = where.getWhere().getRow();
        int c = where.getWhere().getColumn();

        if(name.charAt(0) == 'B'){
            if(where.orientation == 'U'){
                map.put(new Coordinate(r,c+1),0);
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r+1,c+i),i+1);
                }
            }else if(where.orientation == 'R'){
                map.put(new Coordinate(r+1,c+1),0);
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r+i,c),i+1);
                }
            }else if(where.orientation == 'D'){
                map.put(new Coordinate(r+1,c+1),0);
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r,c+i),3-i);
                }
            }else{
                map.put(new Coordinate(r+1,c),0);
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r+i,c+1),3-i);
                }
            }
        }else{
            if(where.orientation == 'U'){
                for(int i = 0; i <4; i++){
                    map.put(new Coordinate(r+i,c),i);
                }
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r+2+i,c+1),i+4);
                }
            }else if(where.orientation == 'R'){
                for(int i = 0; i <4; i++){
                    map.put(new Coordinate(r,c+1+i),3-i);
                }
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r+1,c+i),6-i);
                }
            }else if(where.orientation == 'D'){
                for(int i = 0; i < 3; i++){
                    map.put(new Coordinate(r+i,c),6-i);
                }
                for(int i = 0; i <4; i++){
                    map.put(new Coordinate(r+1+i,c+1),3-i);
                }
            }else{
                for(int i = 0; i <3; i++){
                    map.put(new Coordinate(r,c+2+i),i+4);
                }
                for(int i = 0; i <4; i++){
                    map.put(new Coordinate(r+1,c+i),i);
                }
            }
        }
        return map;
    }

    @Override
    public String getName() {
        return name;
    }


}
