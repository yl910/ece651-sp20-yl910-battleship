package edu.duke.yl910.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public class ComputerPlayer extends TextPlayer {
    ArrayList<Placement> arrayList = new ArrayList<>();


    public ComputerPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, V2ShipFactory shipFactory) {
        super(name, theBoard, inputSource, out, shipFactory);
        arrayList.add(new Placement("a0h"));//s
        arrayList.add(new Placement("b0h"));//s
        arrayList.add(new Placement("c0h"));//d
        arrayList.add(new Placement("d0h"));//d
        arrayList.add(new Placement("e0h"));//d
        arrayList.add(new Placement("g1u"));
        arrayList.add(new Placement("h6r"));
        arrayList.add(new Placement("k2d"));
        arrayList.add(new Placement("n3r"));
        arrayList.add(new Placement("s5r"));

    }

    @Override
    public void doPlacementPhase() throws IOException{
        for(String ship:shipsToPlace) {
            this.doOnePlacement(ship, shipCreationFns.get(ship),false);
        }
    }

    @Override
    public Ship<Character> doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn, boolean move) throws IOException {
        Ship<Character> s = createFn.apply(arrayList.get(0));
        arrayList.remove(0);
        theBoard.tryAddShip(s);
        out.print("Current Ocean:\n");
        out.print(view.displayMyOwnBoard());
        return s;
    }

    @Override
    public void playOneTurn (BattleShipBoard<Character> enemyBoard, TextPlayer enemy) throws IOException{
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        out.print("Player " +this.name+"'s turn:\n");
        String myHeader = "Your ocean";
        String enemyHeader = "Player "+enemy.name+"'s ocean";
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView,myHeader,enemyHeader));
        Coordinate fireAt;
        String row = String.valueOf((char)((int)(Math.random()*20) + 65));
        String col = String.valueOf((int)(Math.random()*10));
        fireAt = new Coordinate(row+col);
        Ship<Character> ship = enemy.theBoard.fireAt(fireAt);
        if(ship != null){
            out.print("You hit a "+ship.getName()+"!\n");
        }
        else{
            out.print("You missed!\n");
        }
    }
}
