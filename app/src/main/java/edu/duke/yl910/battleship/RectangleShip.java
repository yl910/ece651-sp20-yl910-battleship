package edu.duke.yl910.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class RectangleShip <T> extends BasicShip<T>{

    final String name;
    Coordinate pos;
    char direction;

    public RectangleShip(String name, Coordinate upperLeft,  T data, T onHit) {
        this(name, upperLeft, 1, 1, data, onHit);
        this.pos = upperLeft;
    }
    /**
     * Construct a new rectangle ship
     * @param upperLeft is the position
     * @return *
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height,
                         ShipDisplayInfo<T> myShipDisplayInfo, ShipDisplayInfo<T> enemyShipDisplayInfo) {
//        super(makeCoords(upperLeft,width,height),myShipDisplayInfo, enemyShipDisplayInfo);
        super(name,Coords(upperLeft,width,height),myShipDisplayInfo, enemyShipDisplayInfo);
        this.name = name;
        this.pos = upperLeft;

    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
//        this.pos = upperLeft;
    }



    static HashMap<Coordinate, Integer> Coords(Coordinate upperLeft, int width, int height){
        HashMap<Coordinate, Integer> map = new HashMap<>();
        map.put(upperLeft,0);
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        int r = row + height;
        int c = column + width;
        int index = 1;
        for(int i = row; i < r ; i++){
            for(int j = column; j < c; j++) {
                map.put(new Coordinate(i,j),index++);
            }
        }
        return map;
    }



    public String getName(){
        return name;

    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

}
