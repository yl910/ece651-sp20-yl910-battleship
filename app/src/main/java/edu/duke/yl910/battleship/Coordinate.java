package edu.duke.yl910.battleship;

public class Coordinate {
    private final int row;
    private final int column;


    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public Coordinate(int r, int c){
        this.row = r;
        this.column = c;

    }


    /**
     * Constructs a Coordinate with the specified position
     * @param descr is string of coordinate.
     * @throws IllegalArgumentException if the descr input incorrect.
     */
    public Coordinate(String descr){
        if(descr.length() != 2){
            throw new IllegalArgumentException(
                    "descr must equal to 2, but is " + descr.length());
        }
        descr = descr.toUpperCase();
        char rowLetter = descr.charAt(0);
        if (rowLetter < 'A' || rowLetter > 'Z'){
            throw new IllegalArgumentException(
                    "row letter must range from A-Z, but is " + rowLetter);
        }
        int row = descr.charAt(0) - 65;
        char colLetter = descr.charAt(1);
        if(!Character.isDigit(colLetter)){
            throw new IllegalArgumentException(
                    "col letter must be integer, but is " + colLetter);
        }
        int col = descr.charAt(1) - 48;

        if(col > 9){
            throw new IllegalArgumentException(
                    "col must small than 9, but is " + colLetter);
        }

        this.row = row;
        this.column = col;

    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return toString().hashCode();
    }

    @Override
    public String toString(){
        return "("+row+", " + column+")";
    }
}
