package edu.duke.yl910.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn){
        StringBuilder ans = new StringBuilder("");
        ans.append(makeHeader());
        for(int row = 0; row < toDisplay.getHeight(); row++){
            ans.append((char)(row+65));
            ans.append(" ");
            int col = 0;
            for(; col < toDisplay.getWidth()-1; col++){
//                ans.append(" ");
                if(getSquareFn.apply(new Coordinate(row,col) )== null){
                    ans.append(" ");
                }else{
                    ans.append(getSquareFn.apply(new Coordinate(row,col)));
                }
                ans.append("|");
            }
            if(getSquareFn.apply(new Coordinate(row,col) )== null){
                ans.append(" ");
            }else{
                ans.append(getSquareFn.apply(new Coordinate(row,col)));
            }
            ans.append(" ");
            ans.append((char)(row+65));
            ans.append("\n");
        }

        ans.append(makeHeader());
        return ans.toString(); //this is a placeholder for the moment
    }
    public String displayMyOwnBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForSelf);
    }

    public String displayEnemyBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String enemyBoard =  enemyView.displayEnemyBoard();
        String myBoard = this.displayMyOwnBoard();
        StringBuilder allin = new StringBuilder();
        String[] enemyLines = enemyBoard.split("\n");
        String[] myLines = myBoard.split("\n");

        int w = this.toDisplay.getWidth();
        int startCol = 2*w +19;
        for(int i = 0; i < 5; i++){
            allin.append(" ");
        }
        allin.append(myHeader);
        for(int i = 0; i < startCol+1; i++){
            allin.append(" ");
        }
        allin.append(enemyHeader);
        allin.append("\n");
        for(int i = 0; i < enemyLines.length; i++){

            allin.append(myLines[i]);
            if(i==0 || i == enemyLines.length - 1){
                allin.append("  ");
            }
            for(int j = 0; j < startCol-w;j++){
                allin.append(" ");
            }
            allin.append(enemyLines[i]);
            allin.append("\n");
        }
        return allin.toString();
    }



    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }
}

