package edu.duke.yl910.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;

public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    int move_Remaining = 3;
    int sonar_scan = 3;

    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, V2ShipFactory shipFactory){
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        setupShipCreationMap();
        setupShipCreationList();
    }

    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", shipFactory::makeSubmarine);
        shipCreationFns.put("Destroyer", shipFactory::makeDestroyer);
        shipCreationFns.put("Battleship", shipFactory::makeBattleship);
        shipCreationFns.put("Carrier", shipFactory::makeCarrier);

    }

    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }
    //read placement from user
    public Placement readPlacement(String prompt) throws IOException {
        out.print(prompt+"\n");
        String s = inputReader.readLine();
        if(s == null){
            throw new EOFException("produce EOF immediately");
        }
        return new Placement(s);
    }

    public Ship<Character> doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn, boolean move) throws IOException {
        Ship<Character> s;
        while(true){
            try{
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                s = createFn.apply(p);
                String problem = theBoard.tryAddShip(s);
                if(problem != null){
                    out.print(problem+"\n");
                    out.print("Please enter again!\n");
                }else
                    break;
            }catch (IllegalArgumentException e){
                out.print(e.getMessage()+"\n");
            }

        }
        //if move is true means do not need to place a coordinate, we just move previous ship
        if(!move)
            out.print(view.displayMyOwnBoard());
        return s;
    }
    public void doPlacementPhase() throws IOException {
        //display the starting (empty) board
        out.print(view.displayMyOwnBoard());
        //print the instructions message
        for(String ship:shipsToPlace) {
            out.println("--------------------------------------------------------------------------------");
            out.print(view.displayMyOwnBoard());
            out.println("--------------------------------------------------------------------------------");
            out.println("Player " + name + ": you are going to place the following ships (which are all\n" +
                    "rectangular). For each ship, type the coordinate of the upper left\n" +
                    "side of the ship, followed by either H (for horizontal) or V (for\n" +
                    "vertical).  For example M4H would place a ship horizontally starting\n" +
                    "at M4 and going to the right.  You have\n" +
                    "\n" +
                    "2 \"Submarines\" ships that are 1x2 \n" +
                    "3 \"Destroyers\" that are 1x3\n" +
                    "3 \"Battleships\" that are 1x4\n" +
                    "2 \"Carriers\" that are 1x6\n");
            this.doOnePlacement(ship, shipCreationFns.get(ship),false);
            out.print("Current Ocean:\n");
        }
    }




    public void playOneTurn (BattleShipBoard<Character> enemyBoard, TextPlayer enemy) throws IOException{
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        out.print("Player " +this.name+"'s turn:\n");
        String myHeader = "Your ocean";
        String enemyHeader = "Player "+enemy.name+"'s ocean";
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyView,myHeader,enemyHeader));
        String action;
        while(true){
            // add three new actions for player
            while(true){
                out.print("F Fire at a square\n"+
                        "M Move a ship to another square (" + move_Remaining+" remaining)\n"+
                        "S Sonar scan ("+sonar_scan+" remaining)\n"+
                        "\nPlayer "+this.name +", what would you like to do?\n");
                action = inputReader.readLine();
                try{
                    if(checkInput(action)){
                        break;
                    }
                }catch(IllegalArgumentException e){
                    out.print(e.getMessage()+"\n");
                }
            }
            //change the user input to upper case
            char a = Character.toUpperCase(action.charAt(0));
            if((move_Remaining == 0 && sonar_scan == 0) || a == 'F'){
                fireOperation(enemy);
                break;
            }else if(a == 'M'){
                if(move_Remaining != 0){
                    moveOperation(enemy);
                    move_Remaining--;
                    break;
                }else{
                    out.print("You have run of the chances to move");
                }
            }else{
                if(sonar_scan != 0){
                    scanOperation(enemy);
                    sonar_scan--;
                    break;
                }else{
                    out.print("You have run of the chances to scan");
                }
            }
        }
    }

    public void scanOperation(TextPlayer enemy) throws IOException{
        Coordinate occupy;
        while(true){
            out.print("Please enter the center coordinate you want to set sonar: \n");
            String s = inputReader.readLine();
            try{
                occupy = new Coordinate(s);
                break;
            }catch (Exception e){
                out.print(e.getMessage()+"\n");
            }
        }
        int[] res = enemy.theBoard.setSonar(occupy);
        //print part
        out.print("Submarines occupy "+res[0]+" squares\n" +
                "Destroyers occupy "+res[1]+" squares\n" +
                "Battleships occupy "+res[2]+" squares\n" +
                "Carriers occupy "+res[3]+" squares\n");
    }
    

    public void moveOperation(TextPlayer enemy) throws IOException{

        //Iterate to find which ship need to be moved
        BasicShip<Character> oldShip;
        do {
            out.print("Please enter a ship you want to move: \n");
            String s = inputReader.readLine();
            Coordinate occupy = new Coordinate(s);
            oldShip = (BasicShip<Character>) this.theBoard.findShip(occupy);
        } while (oldShip == null);
        BasicShip<Character> newShip = (BasicShip<Character>) this.doOnePlacement(oldShip.getName(),shipCreationFns.get(oldShip.getName()),true);
        this.theBoard.move_ship(oldShip,newShip,enemy);
    }

    private void fireOperation(TextPlayer enemy) throws IOException{
        out.print("Please enter a coordinate you want to fire at: \n");
        Coordinate coordinate;

        while(true){
            String s = inputReader.readLine();
            if(s == null) {
                throw new EOFException(
                        "Receive an EOF when reading fire coordinate and " +
                                "there should not be that way.");
            }
            try{
                coordinate = new Coordinate(s);
                break;
            }catch (IllegalArgumentException e){
                out.print(e.getMessage()+"\n");
            }
        }
        Ship<Character> ship = enemy.theBoard.fireAt(coordinate);
        if(ship != null){
            out.print("You hit a "+ship.getName()+"!\n");
        }
        else{
            out.print("You missed!\n");
        }
    }
    private boolean checkInput(String s){
        if(s.length() != 1){
            throw new IllegalArgumentException("The input length should be 1, but it is "+s.length());
        }
        char action = Character.toUpperCase(s.charAt(0));
        return action == 'F' || action == 'M' || action == 'S';
    }
}