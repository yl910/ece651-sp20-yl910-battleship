package edu.duke.yl910.battleship;

public class Placement {
   final Coordinate where;
   final Character orientation;

    public Character getOrientation() {
        return orientation;
    }

    public Coordinate getWhere() {
        return where;
    }

    public Placement(Coordinate coord, Character o){
        this.where = coord;
        this.orientation = Character.toUpperCase(o);
    }

    /**
     * Constructs a Placement with the specified position
     * @param descr is string of coordinate and orientation.
     * @throws IllegalArgumentException if the descr input incorrect.
     */
    public Placement(String descr){
        if(descr.length() != 3){
            throw new IllegalArgumentException(
                    "descr length should equal to 3, but is " + descr.length());
        }

        this.where = new Coordinate(descr.substring(0,2));
        char orien = Character.toUpperCase(descr.charAt(2));
        if(orien == 'V'){
            this.orientation = orien;
        }else if(orien == 'H'){
            this.orientation = orien;
        }else if(orien == 'U') {
            this.orientation = orien;
        }else if(orien == 'L') {
            this.orientation = orien;
        }else if(orien == 'R') {
            this.orientation = orien;
        } else if(orien == 'D') {
            this.orientation = orien;
        }else{
            throw new IllegalArgumentException(
                    "orientation should be V/H/U/L/R/D, but is " + orien);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return where.getRow() == p.where.getRow() && where.getColumn()
                    == p.where.getColumn() && orientation == p.orientation;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return toString().hashCode();
    }


    @Override
    public String toString(){
        return "("+orientation+")";
    }
}
