package edu.duke.yl910.battleship;

import java.util.HashMap;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T>{

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        // TODO Auto-generated method stub

        int col = theBoard.getWidth();
        int row = theBoard.getHeight();

        for (Coordinate c : theShip.getCoordinates()) {
            if (c.getRow() >= row) {
                return "That placement is invalid: the ship goes off the bottom of the board.";
            } else if (c.getColumn() >= col) {
                return "That placement is invalid: the ship goes off the right of the board.";
            } else if (c.getColumn() < 0) {
                return "That placement is invalid: the ship goes off the left of the board.";
            } else if (c.getRow() < 0) {
                return "That placement is invalid: the ship goes off the top of the board.";
            }
        }
        return null;
    }
}
