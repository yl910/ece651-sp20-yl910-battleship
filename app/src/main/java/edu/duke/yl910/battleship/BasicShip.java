package edu.duke.yl910.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T>{
    String name;
    Coordinate pos;
    char direction;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected HashMap<Coordinate, Integer> blockRecord;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

//    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
//        this.myDisplayInfo = myDisplayInfo;
//        this.enemyDisplayInfo = enemyDisplayInfo;
//        myPieces = new HashMap<>();
//        for (Coordinate c :where){
//            myPieces.put(c,false);
//        }
//    }

    public BasicShip(String name,HashMap<Coordinate,Integer> record, ShipDisplayInfo<T> myDisplayInfo,
                     ShipDisplayInfo<T> enemyDisplayInfo){
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        myPieces = new HashMap<>();
        blockRecord = new HashMap<>();
        for(Coordinate c: record.keySet()){
            myPieces.put(c,false);
            blockRecord.put(c,record.get(c));
        }
        this.name = name;
    }

    //helper method to check if the coordinate is not in this ship
    protected void checkCoordinateInThisShip(Coordinate c) {
        //not contain this coordinate
        if(!occupiesCoordinates(c)){
            throw new IllegalArgumentException("("+c.getRow() +","+
                    c.getColumn()+") not occupied");
        }
    }

    //judge if this coordinate belongs to this ship
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    //judge if the ship already sunk
    @Override
    public boolean isSunk() {
        for(Coordinate c:myPieces.keySet()){
            if(!wasHitAt(c)){
                return false;
            }
        }
        return true;
    }

    //put this ship into board and mark the hited piece
    @Override
    public void recordHitAt(Coordinate where) {
        //coordinate contains
        checkCoordinateInThisShip(where);
        myPieces.put(where,true);
    }

    //judge if this piece is hit
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    //
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip){
        //return the expression of this coordinate, if hit, it is *
        if(myShip){
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }else
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

    //iterate each coordinate
    @Override
    public Iterable<Coordinate> getCoordinates(){
        return myPieces.keySet();
    }

    public HashMap<Coordinate, Boolean> getMyPieces(){
        return myPieces;
    }

    public HashMap<Coordinate, Integer> getBlockRecord(){
        return blockRecord;
    }

}
