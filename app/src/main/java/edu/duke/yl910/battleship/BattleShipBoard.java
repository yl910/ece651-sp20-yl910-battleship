package edu.duke.yl910.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T>{
    private final int width;
    private final int height;
    final ArrayList<Ship<T>> myShips;
    HashSet<Coordinate> enemyMisses;
    private final PlacementRuleChecker<T> placementChecker;
    final T missInfo;


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }



    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementChecker, T missInfo){
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }

        this.width = w;
        this.height = h;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<>();
        this.missInfo = missInfo;
    }

    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int w, int h, T missInfo){
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)),missInfo);
    }

    //which for now will add the ship to the list and return true.
    public String tryAddShip(Ship<T> toAdd){
        String placementProblem = placementChecker.checkPlacement(toAdd, this);
        if (placementProblem == null) {   //<--oops, autocompleted wrong variable...
            myShips.add(toAdd);
            return null;
        }else{
            return placementProblem;
        }

    }
    //This method takes a Coordinate, and sees
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where,true);
    }

    protected T whatIsAt(Coordinate where, boolean isSelf){
        if(!isSelf && enemyMisses.contains(where)){
            return missInfo;
        }

        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    public Ship<T> fireAt(Coordinate c) {
        for(Ship<T> s:myShips){
            //fair at
            if(s.occupiesCoordinates(c)){
                s.recordHitAt(c);
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    //check if all the ships have sunk
    public boolean checkLost(){
        for(Ship<T> s:myShips){
            if(!s.isSunk()){
                return false;
            }
        }
        return true;
    }

    ////version 2////
    //find this coordinate belongs to which ship
    public Ship<T> findShip(Coordinate occupy){
        //Iterate to find which ship need to be moved
        Ship<T> ship = null;
        for(Ship<T> s:myShips){
            if(s.occupiesCoordinates(occupy)){
                ship = s;
            }
        }
        return ship;
    }

    @Override
    public void move_ship(Ship<T> oldS, Ship<T> newS, TextPlayer enemy) {
        for(Coordinate c:oldS.getMyPieces().keySet()){
            //if true, means has been fired
            if(oldS.getMyPieces().get(c)){
                int index = oldS.getBlockRecord().get(c);
                //iterate to find this index block
                for(Coordinate nc: newS.getBlockRecord().keySet()){
                    //find it
                    if(newS.getBlockRecord().get(nc) == index){
                        newS.getMyPieces().put(nc,true);
                        break;
                    }
                }
            }
        }
        myShips.remove(oldS);
        myShips.add(newS);
    }

    public int[] setSonar(Coordinate coordinate){
        int row = coordinate.getRow() - 3;
        int col = coordinate.getColumn();
        int control = 0;
        int[] res = new int[4];// the order is s,d,b,c
        //used to judge the half of triangle has been traversed
        boolean half = false;
        for(int i = row; i>=0 && i< height && i < row+7; i++){
            for(int j = col - control; j >= 0 && j < width && j < col+control+1; j++){
                Ship<T> tmp = findShip(new Coordinate(i,j));
                if(tmp != null){
                    switch (tmp.getName().charAt(0)){
                        case 'S': res[0]++;
                            break;
                        case 'D': res[1]++;
                            break;
                        case 'B': res[2]++;
                            break;
                        case 'C': res[3]++;
                    }
                }
            }
            if(!half && control<3){
                control++;
            }else{
                half = true;
                control --;
            }
        }
        return res;
    }
}
