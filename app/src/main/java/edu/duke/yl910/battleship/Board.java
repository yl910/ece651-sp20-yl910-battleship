package edu.duke.yl910.battleship;

import java.util.HashMap;

public interface Board<T> {
    //obtain the width of board
    public int getWidth();
    //obtain the height of the board
    public int getHeight();
    /**
     * Add the ship to the arraylist and return null.
     * @param toAdd is the ship to be added
     * @return true if the add operation is correctly done, false otherwise
     */

    public String tryAddShip(Ship<T> toAdd);
    /**
     * find the representation of the coordinate of self
     * @param where is the coordinate of the ship
     * @return * if hit, character if it is ship, X if miss
     */
    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    /**
     * fire the ship
     * @param c is the coordinate of the ship
     * @return * if hit, return hit ship, else return null
     */
    public Ship<T> fireAt(Coordinate c);

    /**
     * find if the ship exist in this coordinate
     * @param occupy is the coordinate of the ship
     * @return * if hit, return hit ship, else return null
     */
    public Ship<T> findShip(Coordinate occupy);
    //move ship
    /**
     * move the ship from old place to new place
     * @param oldS is the origin ship, newS is new ship
     * @return * if hit, return hit ship, else return null
     */
    public void move_ship(Ship<T> oldS, Ship<T> newS, TextPlayer enemy);
    //set sonar

    /**
     * set sonar
     * @param coordinate is the position of the sonar
     * @return * return list of all the occupied ships
     */
    public int[] setSonar(Coordinate coordinate);
}
