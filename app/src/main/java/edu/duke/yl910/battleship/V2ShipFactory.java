package edu.duke.yl910.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character>{

    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        RectangleShip<Character> rectangleShip;
        if(where.orientation == 'H'){
            rectangleShip = new RectangleShip<Character>(name, where.getWhere(), h, w, letter, '*');
            rectangleShip.direction = 'H';
        }else {
            rectangleShip = new RectangleShip<Character>(name, where.getWhere(), w, h, letter, '*');
            rectangleShip.direction = 'V';
        }
        return rectangleShip;
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        if(where.orientation != 'H' && where.orientation != 'V'){
            throw new IllegalArgumentException("The direction of this ship is not correct\n");
        }
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        if(where.orientation != 'H' && where.orientation != 'V'){
            throw new IllegalArgumentException("The direction of this ship is not correct\n");
        }
        return createShip(where, 1, 4, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        if(where.orientation != 'U' && where.orientation != 'D' && where.orientation !='R' && where.orientation != 'L'){
            throw new IllegalArgumentException("The direction of this ship is not correct\n");
        }
        return new NotRectangleShip<>("Carrier",where,'c','*');
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        if(where.orientation != 'U' && where.orientation != 'D' && where.orientation !='R' && where.orientation != 'L'){
            throw new IllegalArgumentException("The direction of this ship is not correct\n");
        }
        return new NotRectangleShip<>("Battleship",where,'b','*');
    }
}
