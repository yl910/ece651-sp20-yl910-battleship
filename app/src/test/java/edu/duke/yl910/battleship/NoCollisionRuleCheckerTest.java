package edu.duke.yl910.battleship;
import org.junit.jupiter.api.Test;
import java.util.HashSet;
import static org.junit.jupiter.api.Assertions.*;
public class NoCollisionRuleCheckerTest {

    @Test
    public void test_collision(){
        V1ShipFactory vs1 = new V1ShipFactory();
        Placement p1 = new Placement("A1V");
        BattleShipBoard<Character> battleShipBoard = new BattleShipBoard<>(3,4,'X');
        assertEquals(null,battleShipBoard.tryAddShip(vs1.createShip(p1, 1,2,'C',"Carr")));

//        V1ShipFactory vs2 = new V1ShipFactory();
        Placement p2 = new Placement("B1V");
        assertEquals("That placement is invalid: the ship goes off the right of the board.",
                battleShipBoard.tryAddShip(vs1.createShip(p2, 1,2,'C',"Carr")));

    }
}
