package edu.duke.yl910.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class RectangleShipTest {

//    @Test
//    public void test_makeCoords() {
//        HashSet<Coordinate> set = RectangleShip.makeCoords(new Coordinate(1, 2), 1, 3);
//        HashSet<Coordinate> expected = new HashSet<>();
//        expected.add(new Coordinate(1,2));
//        expected.add(new Coordinate(2,2));
//        expected.add(new Coordinate(3,2));
//        assertEquals(expected, set);
//    }

    @Test
    public void test_hit() {
        RectangleShip<Character> rectangleShip = new RectangleShip<>("Destroyer",new Coordinate(1,2),1,3,'s','*');
        rectangleShip.recordHitAt(new Coordinate("B2"));
        rectangleShip.recordHitAt(new Coordinate("C2"));
        rectangleShip.recordHitAt(new Coordinate("D2"));
        assertTrue(rectangleShip.isSunk());

    }



}
