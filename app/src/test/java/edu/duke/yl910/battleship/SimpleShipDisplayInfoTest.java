package edu.duke.yl910.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleShipDisplayInfoTest {
    @Test
    //copy
    void testGetInfo() {
        SimpleShipDisplayInfo<Character> display = new SimpleShipDisplayInfo<>('s', '*');
        Character infoTrue = display.getInfo(new Coordinate(1, 1), true);
        Character infoFalse = display.getInfo(new Coordinate(1, 1), false);
        assertEquals('*', infoTrue);
        assertEquals('s', infoFalse);

    }

}
