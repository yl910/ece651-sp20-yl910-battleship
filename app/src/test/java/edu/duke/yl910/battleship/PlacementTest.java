package edu.duke.yl910.battleship;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class PlacementTest {

    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(1, 3);
        Coordinate c4 = new Coordinate(3, 2);

        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c2, 'H');
        Placement p4 = new Placement(c3, 'v');
        Placement p5 = new Placement(c2, 'h');
        Placement p6 = new Placement(c4, 'H');

        assertEquals(p1, p2);
        assertEquals(p3, p5);
        assertNotEquals(p2,p3);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, p4);
        assertNotEquals(p1, p3);
        assertNotEquals(p2, p6);
        assertNotEquals(p1, "(1,2)");
    }

    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        assertEquals(p1.hashCode(), p2.hashCode());

    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3H");
        assertEquals(1, p1.where.getRow());
        assertEquals(3, p1.where.getColumn());
        assertEquals('H',p1.orientation);
        Placement p2 = new Placement("D5v");
        assertEquals(3, p2.where.getRow());
        assertEquals(5, p2.where.getColumn());
        assertEquals('V',p2.orientation);
        Placement p3 = new Placement("A9h");
        assertEquals(0, p3.where.getRow());
        assertEquals(9, p3.where.getColumn());
        assertEquals('H', p3.orientation);
        Placement p4 = new Placement("Z0V");
        assertEquals(25, p4.where.getRow());
        assertEquals(0, p4.where.getColumn());
        assertEquals('V',p4.orientation);

    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("00H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A4H3"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0VV"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A2Y"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12V"));
    }
}
