package edu.duke.yl910.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
    @Test
    public void test_display_empty_2by2() {
        Board<Character> b1 = new BattleShipBoard<Character>(2, 2,'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  |  A\n"+
                        "B  |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_3by2(){
        Board<Character> b1 = new BattleShipBoard<Character> (3, 2,'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader= "  0|1|2\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected=
                expectedHeader+
                        "A  | |  A\n"+
                        "B  | |  B\n"+
                        expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

    }

    @Test
    public void test_display_empty_3by5(){
        Board<Character> b1 = new BattleShipBoard<Character>(3,5,'X');
        BoardTextView view = new BoardTextView(b1);
        String expectedHeader = "  0|1|2\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected= expectedHeader +
                "A  | |  A\n"+
                "B  | |  B\n"+
                "C  | |  C\n"+
                "D  | |  D\n"+
                "E  | |  E\n"+
                expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20,'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27,'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }



    @Test
    public void test_display_2by3(){
        Board<Character> b1 = new BattleShipBoard<Character>(2,3,'X');
        BoardTextView view = new BoardTextView(b1);
//        b1.tryAddShip(new BasicShip(new Coordinate(0,1)));
        b1.tryAddShip(new RectangleShip<Character>("submarine",new Coordinate(0,1),'s','*'));
//        b1.tryAddShip(new BasicShip(new Coordinate(1,1)));
        b1.tryAddShip(new RectangleShip<Character>("submarine", new Coordinate(1,1),'s','*'));

        String expectedHeader = "  0|1\n";
        assertEquals(expectedHeader, view.makeHeader());
        String expected= expectedHeader +
                "A  |s A\n"+
                "B  |s B\n"+
                "C  |  C\n"+
                expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());

    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<Character>(w, h,'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }


    @Test
    public void test_display_empty_10by20(){
        Board<Character> b1 = new BattleShipBoard<Character> (10, 20,'X');
        BoardTextView myview = new BoardTextView(b1);
        Board<Character> b2 = new BattleShipBoard<>(10,20,'X');
        BoardTextView eview = new BoardTextView(b2);
        String expectedHeader= "  0|1|2|3|4|5|6|7|8|9\n";
        assertEquals(expectedHeader, myview.makeHeader());
        String expected=
                expectedHeader+
                        "A  | |  A\n"+
                        "B  | |  B\n"+
                        expectedHeader;
        assertEquals(expected, eview.displayMyBoardWithEnemyNextToIt(eview,"Your Ocean","Player B's ocean"));

    }

}
