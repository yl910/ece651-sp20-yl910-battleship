package edu.duke.yl910.battleship;

import org.junit.jupiter.api.Test;
import java.util.HashSet;
import static org.junit.jupiter.api.Assertions.*;

public class InBoundsRuleCheckerTest {

    @Test
    public void test_bound(){
        V1ShipFactory vs1 = new V1ShipFactory();
        Placement p1 = new Placement("A2H");
        BattleShipBoard<Character> battleShipBoard = new BattleShipBoard<>(2,4,'X');
        assertEquals("That placement is invalid: the ship goes off the right of the board.",
                battleShipBoard.tryAddShip(vs1.createShip(p1, 3,4,'C',"Carr")));
    }

}
