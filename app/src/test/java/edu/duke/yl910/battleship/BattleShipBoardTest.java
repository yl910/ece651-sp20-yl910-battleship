package edu.duke.yl910.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_empty_board(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(2,3,'X');
        Character[][] expect = new Character[3][2];
        checkWhatIsAtBoard(b1, expect);
    }

    private <Character> void checkWhatIsAtBoard(BattleShipBoard<java.lang.Character> b, java.lang.Character[][] expected){
        int w = b.getWidth();
        int h = b.getHeight();
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                assertEquals(b.whatIsAtForSelf(new Coordinate(i, j)), expected[i][j]);
            }
        }
    }

    @Test
    public void test_add_ships(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(2,3,'X');
        Coordinate where = new Coordinate(2,3);
        RectangleShip<Character> s = new RectangleShip<Character>("BattleShip",where, 's', '*');
//        assert(b1.tryAddShip(s));
        assertEquals('s',s.getDisplayInfoAt(where,true));//not sure about true
    }




}