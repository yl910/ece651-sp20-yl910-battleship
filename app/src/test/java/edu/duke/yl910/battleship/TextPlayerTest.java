package edu.duke.yl910.battleship;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

public class TextPlayerTest {
    @Test
    void test_do_one_placement() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
        V2ShipFactory shipFactory = new V2ShipFactory();
        player.doOnePlacement("Submarine", shipFactory::makeSubmarine,false);
        player.doOnePlacement("Destroyer", shipFactory::makeDestroyer,false);
        player.doOnePlacement("Carrier", shipFactory::makeCarrier,false);
        String expectedHeader= "  0|1|2|3|4|5|6|7|8|9\n";
        assertEquals(expectedHeader, player.view.makeHeader());
        String expected=
                expectedHeader+
                        "A  | | | |c| | | | |  A\n"+
                        "B  | |s| |c| | | | |  B\n"+
                        "C  | |s| |c| | | | |  C\n"+
                        "D  | | | |c| | | | |  D\n"+
                        "E  | | | |c| | | | |  E\n"+
                        "F  | | | |c| | | | |  F\n"+
                        "G  | | | | | | | | |  G\n"+
                        "H  | | | | | | | | |  H\n"+
                        "I  | | | | | | | | |  I\n"+
                        "J  | | | | | | | | |  J\n"+
                        "K  | | | | | | | | |  K\n"+
                        "L  | | | | | | | | |  L\n"+
                        "M  | | | | | | | | |  M\n"+
                        "N  | | | | | | | | |  N\n"+
                        "O  | | | | | | | | |  O\n"+
                        "P  | | | | | | | | |  P\n"+
                        "Q  | | | | | | | | |  Q\n"+
                        "R  | | | | | | | | |  R\n"+
                        "S  | | | | | | | | |  S\n"+
                        "T  | | | | | | | | |  T\n"+
                        expectedHeader;
        assertEquals(expected, player.view.displayMyOwnBoard());
    }

    private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h,'X');

        V2ShipFactory shipFactory = new V2ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

}
